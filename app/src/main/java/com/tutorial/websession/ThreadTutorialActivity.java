package com.tutorial.websession;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by shivakanthrai on 01/06/17.
 */

public class ThreadTutorialActivity extends AppCompatActivity {

    private EditText timeEditText;
    private Button executeButton;
    private static ProgressDialog progressDialog;
    private static String timeString;

    private Runnable sampleRunnable = new Runnable() {
        @Override
        public void run() {
            Integer timeInSecs = Integer.parseInt(timeString);
            long count = (timeInSecs*10000000L);

            for(long i = 0; i < count;i++){

            }

            new Handler().postDelayed(sampleRunnable,5000L);

        }
    };


    private Handler sampleThreadHandler = new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            progressDialog.dismiss();
        }
    };
    private HandlerThread sampleThread;
    private Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_async_task_tutorial);
        initalizeUIElements();
        addEventListeners();

    }

    private void initalizeUIElements() {

        timeEditText = (EditText) findViewById(R.id.et_time);
        executeButton = (Button) findViewById(R.id.bt_execute);
    }

    private void addEventListeners() {
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSampleThread();
            }
        });
    }

    private void showProgress(){
        progressDialog = ProgressDialog.show(ThreadTutorialActivity.this,"Thread Task Example","Waiting for "+timeEditText.getText().toString()+" seconds");
    }

    private void createSampleThread() {
        showProgress();
        timeString = timeEditText.getText().toString();
        sampleThread =


        //Example of using handler for non UI thread
        new HandlerThread(""){
            @Override
            public void run() {
                Integer timeInSecs = Integer.parseInt(timeString);
                long count = (timeInSecs*10000000L);

                for(long i = 0; i < count;i++){

                }

                //TO DO Why do we need to use the handler here instead of doing UI operations here directl
                 progressDialog.dismiss();
                handler.sendEmptyMessage(0);

            }
        };


        handler = new Handler(sampleThread.getLooper()){
            @Override
            public void handleMessage(Message msg) {

            }
        };

        sampleThread.start();


        //Example for Handler.post.... method
        new Handler().postDelayed(sampleRunnable,5000L);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}


