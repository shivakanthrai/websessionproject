package com.tutorial.websession.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tutorial.websession.R;

/**
 * Created by shivakanthrai on 12/07/17.
 */

public class SampleDialogFragment extends DialogFragment {

    public static String SELECTED_COLOR = "Red";
    private String[] colorArray = new String[]{"Red","Blue","Green"};
    private SampleDialogFragmentListener sampleDialogFragmentListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_signin, null))
                // Add action buttons
                .setPositiveButton(R.string.signin, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SampleDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();

    }

    public void setSampleDialogFragmentListener(SampleDialogFragmentListener sampleDialogFragmentListener) {
        this.sampleDialogFragmentListener = sampleDialogFragmentListener;
    }

    public interface SampleDialogFragmentListener{
        void onColorPick(String color);
    }
}
