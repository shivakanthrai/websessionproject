package com.tutorial.websession.dialog;

import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tutorial.websession.R;

import java.util.List;

/**
 * Created by shivakanthrai on 12/07/17.
 */

public class DevicesDialogFragment extends DialogFragment {

    private String[] deviceNames;
    private List<BluetoothDevice> bluetoothDevices;
    private SampleDialogFragmentListener sampleDialogFragmentListener;

    public void setDeviceDialogListener(DeviceDialogListener deviceDialogListener) {
        this.deviceDialogListener = deviceDialogListener;
    }

    public DeviceDialogListener deviceDialogListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select a device");
        builder.setItems(deviceNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deviceDialogListener.connectWithDevice(bluetoothDevices.get(i));
            }
        });

        return builder.create();

    }

    public void setSampleDialogFragmentListener(SampleDialogFragmentListener sampleDialogFragmentListener) {
        this.sampleDialogFragmentListener = sampleDialogFragmentListener;
    }

    public interface SampleDialogFragmentListener{
        void onColorPick(String color);
    }

    public void setBluetoothDevices(List<BluetoothDevice> bluetoothDevices) {
        this.bluetoothDevices = bluetoothDevices;
        deviceNames = new String[bluetoothDevices.size()];

        for (int i=0 ; i < bluetoothDevices.size();i++) {
            deviceNames[i] = bluetoothDevices.get(i).getName();

        }


    }

    public  interface DeviceDialogListener {
        void connectWithDevice(BluetoothDevice bluetoothDevice);
    }
}
