package com.tutorial.websession;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

/**
 * Created by shivakanthrai on 07/07/17.
 */

public class ProgressDialogExampleActvity extends AppCompatActivity {

    private Button pauseButton;
    private ProgressBar progressBar;
    int progress = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_example);

        pauseButton = (Button) findViewById(R.id.bt_start_pause);
        progressBar = (ProgressBar) findViewById(R.id.pb_progress);

        progressBar.setIndeterminate(false);
        progressBar.setMax(10);




        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*
                if(progressBar.getVisibility() == View.VISIBLE){
                    progressBar.setVisibility(View.INVISIBLE);
                    pauseButton.setText("Start");
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                    pauseButton.setText("Pause");
                }
                */
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(++progress);

                if(progress ==10){
                    progress = 0;
                }
            }
        });
    }
}
