package com.tutorial.websession.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.tutorial.websession.model.Comment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shivakanthrai on 05/07/17.
 */

public class SampleSQLiteOpenHelper extends SQLiteOpenHelper {

    private static SampleSQLiteOpenHelper sInstance;
    private final Context context;
    private final String dbPath;

    public static final String TABLE_COMMENTS = "comments";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_COMMENT = "comment";


    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_COMMENTS + "( " + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_COMMENT
            + " text not null);";

    private SQLiteDatabase db;


    private SampleSQLiteOpenHelper(Context context) {
        super(context, RPDataBaseConstants.DB_NAME, null, RPDataBaseConstants.DB_VERSION);
        this.context = context;
        SQLiteDatabase database = this.getWritableDatabase();
        dbPath = database.getPath();
        this.close();
    }

    /**
     * Get the instance of the DatabaseHelper
     *
     * @param context ApplicationContext
     * @return SampleSQLiteOpenHelper
     */
    public static synchronized SampleSQLiteOpenHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SampleSQLiteOpenHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(DATABASE_CREATE);
        } catch (Exception ex) {

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch(oldVersion) {
            case 1:

            case 2:

            case 3:

        }
    }

    private SQLiteDatabase getSQLiteDatabase() {
        if (db == null)
            db = openDatabaseNoLocalizedCollators();
        return db;
    }

    private SQLiteDatabase openDatabaseNoLocalizedCollators() {
        return SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
    }


    public void createComment(String comment){
        try{
            SQLiteDatabase liteDatabase = getSQLiteDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_COMMENT,comment);
            liteDatabase.insert(TABLE_COMMENTS,null,contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public List<Comment> getAllComments(){
        ArrayList<Comment> comments = new ArrayList<>();
        SQLiteDatabase liteDatabase = getSQLiteDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_COMMENTS, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Comment comment = new Comment();
                    comment.setCommentId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                    comment.setComment(cursor.getString(cursor.getColumnIndex(COLUMN_COMMENT)));
                    comments.add(comment);

                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return comments;



    }



}
