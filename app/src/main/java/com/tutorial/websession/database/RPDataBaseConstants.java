package com.tutorial.websession.database;

/**
 * Created by shivakanthrai on 05/07/17.
 */

class RPDataBaseConstants {
    public static final String DB_NAME = "websession.sqlite";
    public static final int DB_VERSION = 1;
}
