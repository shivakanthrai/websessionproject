package com.tutorial.websession.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by shivakanthrai on 06/06/17.
 */

public class SampleBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, final Intent intent) {

        String name = intent.getStringExtra("name");

        Log.i(SampleBroadcastReceiver.class.getName(),"Received Broadcast from "+name+" !!!");


    }
}
