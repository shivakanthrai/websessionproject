package com.tutorial.websession;

import android.Manifest;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ListViewCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.logging.Logger;

/**
 * Created by shivakanthrai on 07/06/17.
 */

public class UserDictionaryContentProviderExampleActivity extends AppCompatActivity {

    private EditText wordEditText;
    private Button addWordButton;
    private ListView wordsListView;

    private String[] projections = {UserDictionary.Words.WORD};
    private String updateSelection = UserDictionary.Words.WORD + " = ?";
    private String[] updateSelectionArgs = {"Namitha"};
    private String selection = null;
    private String[] selectionArgs = null;
    private String sort = UserDictionary.Words.WORD+" ASC";

    private int[] toFields = {R.id.tv_dictionary_word};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_content_provider_tutorial);

        wordEditText = (EditText) findViewById(R.id.et_new_word);
        addWordButton = (Button) findViewById(R.id.bt_add_word);
        wordsListView = (ListView) findViewById(R.id.lv_words);

        addWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteWordInDictionary();
            }
        });

    }

    private void addWordToDictionary() {
        ContentValues contentValues = new ContentValues();

        contentValues.put(UserDictionary.Words.APP_ID,0);
        contentValues.put(UserDictionary.Words.WORD,wordEditText.getText().toString());
        contentValues.put(UserDictionary.Words.LOCALE,"en_IN");
        contentValues.put(UserDictionary.Words.FREQUENCY,"100");

        getContentResolver().insert(UserDictionary.Words.CONTENT_URI,contentValues);
        getAllDictionaryWords();
        wordEditText.setText("");

    }

    private void updateWordInDictionary() {
        ContentValues contentValues = new ContentValues();

        contentValues.put(UserDictionary.Words.WORD,wordEditText.getText().toString());

        int noOfRowsUpdated = getContentResolver().update(UserDictionary.Words.CONTENT_URI,contentValues,updateSelection,updateSelectionArgs);
        getAllDictionaryWords();
        wordEditText.setText("");

    }

    private void deleteWordInDictionary(){
        String[] args= {wordEditText.getText().toString()};
        getContentResolver().delete(UserDictionary.Words.CONTENT_URI,UserDictionary.Words.WORD+" = ? ",args);
        getAllDictionaryWords();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllDictionaryWords();

    }

    private void  getAllDictionaryWords(){
        try{
            Cursor userDictionaryCursor = getContentResolver().query(UserDictionary.Words.CONTENT_URI,projections,selection,selectionArgs,sort);


            Log.i(getClass().getName(),"WORD --> ====== START ==========");
            while (userDictionaryCursor.moveToNext()){
                Log.i(getClass().getName(),"WORD --> "+userDictionaryCursor.getString(0));
            }
            Log.i(getClass().getName(),"WORD --> ====== END ==========");


            /*  SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this,R.layout.layout_dictionary_row,userDictionaryCursor,projections,toFields,0);
            wordsListView.setAdapter(simpleCursorAdapter);
            simpleCursorAdapter.notifyDataSetChanged();*/

        }catch (Exception e){
            e.printStackTrace();

            // Did  not return any results
        }
    }
}
