package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by shivakanthrai on 16/06/17.
 */

public class UILayoutExampleActivity extends AppCompatActivity {

    LinearLayout parentLinearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_example);
        parentLinearLayout = (LinearLayout) findViewById(R.id.ll_parent_container);


        buildLayoutUsingInflator();
    }

    private void buildLayout() {
        LinearLayout firstLinearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        firstLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        firstLinearLayout.setPadding(getPixelValue(20),getPixelValue(15),0,0);
        firstLinearLayout.setLayoutParams(layoutParams);


        EditText editText = new EditText(this);
        editText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        //editText.setPadding(0,0,0,0);
        editText.setHint(getString(R.string.time_hint));


        Button executeButton = new Button(this);
        executeButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        executeButton.setText(getString(R.string.execute));

        firstLinearLayout.addView(editText);
        firstLinearLayout.addView(executeButton);

        parentLinearLayout.addView(firstLinearLayout);


        LinearLayout secondLinearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams secondLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        secondLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        secondLinearLayout.setPadding(getPixelValue(20),getPixelValue(15),0,0);
        secondLinearLayout.setLayoutParams(secondLayoutParams);


        EditText editText1 = new EditText(this);
        editText1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        //editText1.setPadding(0,0,0,0);
        editText1.setHint(getString(R.string.time_hint));

        secondLinearLayout.addView(editText1);
        parentLinearLayout.addView(secondLinearLayout);
    }

    private void buildLayoutUsingInflator() {
      View view =  getLayoutInflater().inflate(R.layout.layout_async_task_demo,parentLinearLayout,true);
    }


    private int getPixelValue(int dpValue){
        final float scale = getResources().getDisplayMetrics().density;
        int px = (int) (dpValue * scale + 0.5f);
        return px;
    }
}
