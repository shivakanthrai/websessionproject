package com.tutorial.websession;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

/**
 * Created by shivakanthrai on 23/06/17.
 */

public class LocalNotificationExampleActivity extends AppCompatActivity{

    private Button generateNotificationButton;
    private EditText nameEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_local_notification_activity);

        generateNotificationButton = (Button) findViewById(R.id.bt_notification);
        nameEditText = (EditText) findViewById(R.id.et_name);

        generateNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateLocalNotification();
            }
        });


    }

    private void generateLocalNotification() {
        int mId = 1;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

        mBuilder.setSmallIcon(R.drawable.ic_stat_android)
                        .setCustomBigContentView(createCustomNotificationLayout())
                        .setAutoCancel(true);
        // Creates an explicit intent for an Activity in your app
        Intent parentIntent = new Intent(this, LocalNotificationExampleActivity.class);

        Intent childIntent = new Intent(this, HelloActivity.class);
        childIntent.putExtra("name",nameEditText.getText().toString());



        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder = stackBuilder.addParentStack(HelloActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        //stackBuilder = stackBuilder.addNextIntent(parentIntent);
        stackBuilder = stackBuilder.addNextIntent(childIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );



        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(mId,mBuilder.build());

    }


    private RemoteViews createCustomNotificationLayout(){
        RemoteViews customView  = new RemoteViews(getPackageName(),R.layout.layout_notification_custom);
        customView.setTextViewText(R.id.tv_notification_heading,"Something");
        customView.setTextViewText(R.id.tv_notification_description,"Hey");


        // Creates an explicit intent for an Activity in your app
        Intent parentIntent = new Intent(this, LocalNotificationExampleActivity.class);

        Intent childIntent = new Intent(this, HelloActivity.class);
        childIntent.putExtra("name","From button click: " +nameEditText.getText().toString());



        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder = stackBuilder.addParentStack(HelloActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        //stackBuilder = stackBuilder.addNextIntent(parentIntent);
        stackBuilder = stackBuilder.addNextIntent(childIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_ONE_SHOT
                );

        customView.setOnClickPendingIntent(R.id.bt_show,resultPendingIntent);
        return customView;
    }
}
