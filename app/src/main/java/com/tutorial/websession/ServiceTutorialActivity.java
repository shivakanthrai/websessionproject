package com.tutorial.websession;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tutorial.websession.service.ExampleService;

/**
 * Created by shivakanthrai on 02/06/17.
 */

public class ServiceTutorialActivity extends AppCompatActivity {

    private EditText timeEditText;
    private Button executeButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_async_task_tutorial);
        initalizeUIElements();
        addEventListeners();

    }

    private void initalizeUIElements() {

        timeEditText = (EditText) findViewById(R.id.et_time);
        executeButton = (Button) findViewById(R.id.bt_execute);
    }

    private void addEventListeners() {
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSampleService();
            }
        });
    }

    private void createSampleService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        startService(serviceIntent);
        //bindService();
    }
}
