package com.tutorial.websession.singleton;

/**
 * Created by shivakanthrai on 24/06/17.
 */

public class SingletonClass {

    private static SingletonClass instance;

    private SingletonClass(){

    }

    public static SingletonClass getInstance(){

        if(instance == null ){
            instance = new SingletonClass();
        }

        return instance;
    }
}
