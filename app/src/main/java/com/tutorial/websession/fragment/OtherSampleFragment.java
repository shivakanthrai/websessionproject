package com.tutorial.websession.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tutorial.websession.R;

/**
 * Created by shivakanthrai on 14/06/17.
 */

public class OtherSampleFragment extends Fragment {
    private View rootView ;
    private TextView messageTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_other_fragment,container,false);
        messageTextView = (TextView) rootView.findViewById(R.id.tv_message);

        return rootView;

    }

    public void receiveMessage(String msg){
        messageTextView.setText(msg);
    }
}
