package com.tutorial.websession.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tutorial.websession.FragmentExampleActivity;
import com.tutorial.websession.R;

/**
 * Created by shivakanthrai on 14/06/17.
 */

public class SampleFragment extends Fragment{

    View rootView;
    Button executeButton;
    SampleFragmentListener sampleFragmentListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_sample_fragment,container,false);

        sampleFragmentListener = (SampleFragmentListener) getActivity();

        executeButton = (Button) rootView.findViewById(R.id.bt_execute);
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sampleFragmentListener.sayHello("Hello from fragment 1");
            }
        });


        return rootView;
    }


    public interface SampleFragmentListener{
        void sayHello(String message);
    }
}
