package com.tutorial.websession.fragment;


import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.tutorial.websession.R;

/**
 * Created by shivakanthrai on 15/06/17.
 */

public class MasterFragment extends Fragment implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {
    private View rootView;
    private ListView masterListView;
    private MasterFragmentListener masterFragmentListener;
    private String[] listItems = new String[] {"item 1","item 2"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_master_frag,container,false);
        masterListView = (ListView) rootView.findViewById(R.id.lv_master);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,listItems);
        masterListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        masterFragmentListener = (MasterFragmentListener) getActivity();

        masterListView.setOnItemClickListener(this);
        return rootView;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        masterFragmentListener.selectedValue(listItems[i]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        masterFragmentListener.selectedValue(listItems[i]);
    }

    public interface MasterFragmentListener{
        void selectedValue(String value);
    }
}
