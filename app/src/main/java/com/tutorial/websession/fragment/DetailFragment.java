package com.tutorial.websession.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tutorial.websession.R;

/**
 * Created by shivakanthrai on 15/06/17.
 */

public class DetailFragment extends Fragment{

    private View rootView;
    private TextView detailTextView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.layout_detail_fragment,container,false);
        detailTextView = (TextView) rootView.findViewById(R.id.tv_detail);
        return rootView;
    }

    public void showSelectedValue(String value) {
        detailTextView.setText("Selected value is '"+value+"'");
    }
}
