package com.tutorial.websession;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.websession.dialog.DevicesDialogFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

/**
 * Created by shivakanthrai on 13/07/17.
 */

public class BlueToothExampleActivity  extends AppCompatActivity implements DevicesDialogFragment.DeviceDialogListener {

    private static final int REQUEST_ENABLE_BT = 1000;
    private BluetoothAdapter mBluetoothAdapter;

    private Button startButton;
    private Button devicesButton;
    private TextView statusTextView;
    private Set<BluetoothDevice> pairedDevices;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_example);

        startButton = (Button) findViewById(R.id.bt_turn_on);
        devicesButton = (Button) findViewById(R.id.bt_get_devices);
        statusTextView = (TextView) findViewById(R.id.tv_blue_tooth_status);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startStopBlueToothAdapter();
            }
        });

        devicesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPairedDevices();
            }
        });


    }



    private void startStopBlueToothAdapter() {

        if(!mBluetoothAdapter.isEnabled()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }else{
            mBluetoothAdapter.disable();
            statusTextView.setText("Bluetooth is not turned on");
            startButton.setText("Start");
            startButton.setEnabled(true);
            devicesButton.setEnabled(false);
        }

    }

    private void showPairedDevices() {
        pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
            }

            DevicesDialogFragment devicesDialogFragment = new DevicesDialogFragment();
            devicesDialogFragment.setBluetoothDevices(new ArrayList<BluetoothDevice>(pairedDevices));
            devicesDialogFragment.setDeviceDialogListener(this);
            devicesDialogFragment.show(getSupportFragmentManager(),"");

        }else{
            Toast.makeText(this,"No Paired Devices",Toast.LENGTH_LONG).show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!checkIfBlueToothIsAvailable()){
            Toast.makeText(this,"Bluetooth not available on this device",Toast.LENGTH_LONG).show();
            statusTextView.setText("Bluetooth not available on this device");
            startButton.setEnabled(false);
            devicesButton.setEnabled(false);
        }else{
            if (!mBluetoothAdapter.isEnabled()) {
                statusTextView.setText("Bluetooth is not turned on");
                startButton.setText("Start");
                startButton.setEnabled(true);
                devicesButton.setEnabled(false);
            }else{
                statusTextView.setText("Bluetooth is turned on");
                startButton.setEnabled(true);
                startButton.setText("Stop");
                devicesButton.setEnabled(true);
            }
        }
    }

    private boolean checkIfBlueToothIsAvailable() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        }else{
            return true;
        }


    }


    @Override
    public void connectWithDevice(BluetoothDevice bluetoothDevice) {
        Toast.makeText(this,"Connecting to "+bluetoothDevice.getName(),Toast.LENGTH_LONG).show();
        new Handler().post(new ConnectThread(bluetoothDevice));
    }



    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(UUID.randomUUID());
            } catch (IOException e) {
                Log.e(ConnectThread.class.getName(), "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(ConnectThread.class.getName(), "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            manageMyConnectedSocket(mmSocket);
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(ConnectThread.class.getName(), "Could not close the client socket", e);
            }
        }
    }

    private void manageMyConnectedSocket(BluetoothSocket mmSocket) {
        Toast.makeText(this,"Socket Opend",Toast.LENGTH_LONG).show();

    }
}
