package com.tutorial.websession;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tutorial.websession.asynctask.SampleAsynTaskListener;
import com.tutorial.websession.asynctask.SampleAsyncTask;
import com.tutorial.websession.singleton.SingletonClass;

/**
 * Created by shivakanthrai on 31/05/17.
 */

public class AsyncTaskTutorialActivity extends AppCompatActivity implements SampleAsynTaskListener {

    private EditText timeEditText;
    private Button executeButton;
    private ProgressDialog progressDialog;
    private SampleAsyncTask sampleAsyncTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_async_task_tutorial);

        initalizeUIElements();
        addEventListeners();


        SingletonClass singletonClass1 = SingletonClass.getInstance();


    }

    private void initalizeUIElements() {

        timeEditText = (EditText) findViewById(R.id.et_time);
        executeButton = (Button) findViewById(R.id.bt_execute);
    }

    private void addEventListeners() {
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //runDummyTimeConsumingTask();
                createAsyncTask();
            }
        });
    }

    private void createAsyncTask() {
        sampleAsyncTask = new SampleAsyncTask(this);
        sampleAsyncTask.execute(timeEditText.getText().toString());
    }

    private void runDummyTimeConsumingTask() {
        Integer timeInSecs = Integer.parseInt(timeEditText.getText().toString());
        showProgress();
        long count = (timeInSecs*10000000L);

        for(long i = 0; i < count;i++){

        }

    }

    private void showProgress(){
        progressDialog = ProgressDialog.show(AsyncTaskTutorialActivity.this,"Async Task Example","Wainting for "+timeEditText.getText().toString()+" seconds");
    }

    @Override
    public void startingTaskExecution() {
        showProgress();
    }

    @Override
    public void showProgress(String progressMessage) {

    }

    @Override
    public void taskCompleted(String result) {
        progressDialog.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(sampleAsyncTask != null && (sampleAsyncTask.getStatus() == AsyncTask.Status.PENDING ||
                sampleAsyncTask.getStatus() == AsyncTask.Status.RUNNING)){
            sampleAsyncTask.cancel(true);

        }
        super.onDestroy();
    }
}
