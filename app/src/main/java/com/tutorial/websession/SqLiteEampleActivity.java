package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.tutorial.websession.adapter.CommentAdapter;
import com.tutorial.websession.database.SampleSQLiteOpenHelper;
import com.tutorial.websession.model.Comment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shivakanthrai on 05/07/17.
 */

public class SqLiteEampleActivity extends AppCompatActivity {

    private EditText commentEditText;
    private Button addButton;
    private ListView commentsListView;
    private List<Comment> comments = new ArrayList<>();
    private CommentAdapter commentsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite_example);

        commentEditText = (EditText) findViewById(R.id.et_comment);
        addButton = (Button) findViewById(R.id.bt_add_comment);
        commentsListView = (ListView) findViewById(R.id.lv_comments_list);

        comments = SampleSQLiteOpenHelper.getInstance(this).getAllComments();
        commentsAdapter = new CommentAdapter(getApplicationContext(),R.layout.row_comment,comments);
        commentsListView.setAdapter(commentsAdapter);
        commentsAdapter.notifyDataSetChanged();

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addComment();
            }
        });
    }


    private void addComment() {
        SampleSQLiteOpenHelper.getInstance(this).createComment(commentEditText.getText().toString());

        commentsAdapter.clear();
        commentsAdapter.addAll(SampleSQLiteOpenHelper.getInstance(this).getAllComments());
        commentsAdapter.notifyDataSetChanged();
    }

}
