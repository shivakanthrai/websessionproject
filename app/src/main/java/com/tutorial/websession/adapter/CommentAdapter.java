package com.tutorial.websession.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tutorial.websession.R;
import com.tutorial.websession.model.Comment;

import java.util.List;

/**
 * Created by shivakanthrai on 05/07/17.
 */

public class CommentAdapter extends ArrayAdapter<Comment> {

    List<Comment> comments;
    public CommentAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Comment> comments) {
        super(context, resource, comments);

        this.comments = comments;



    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment,parent,false);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.tv_comment);
        textView.setText(comments.get(position).getComment());

        return  convertView;
    }
}
