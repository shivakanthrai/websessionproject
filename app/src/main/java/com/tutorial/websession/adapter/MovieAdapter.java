package com.tutorial.websession.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tutorial.websession.R;
import com.tutorial.websession.model.Movie;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by shivakanthrai on 16/06/17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private Context context;
    private List<Movie> movies = new ArrayList<>();
    int viewCreatedCount = 0;

    public MovieAdapter(Context context, List<Movie> movies){
            this.context = context;
            this.movies = movies;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_movie_row,parent,false);
        MovieViewHolder movieViewHolder = new MovieViewHolder(view);
        viewCreatedCount++;
        Log.i(getClass().getName(),"Views Created -> "+viewCreatedCount);
        return movieViewHolder;
    }



    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {

        Movie movie = movies.get(position);

        if(movie != null){
            holder.nameTextView.setText(movie.getName());
            holder.releaseDateTextView.setText(movie.getYearOfRelease());
            holder.genreTextView.setText(movie.getGenre());
        }

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


    class MovieViewHolder extends RecyclerView.ViewHolder{
        private TextView nameTextView,releaseDateTextView,genreTextView;

        public MovieViewHolder(View view){
            super(view);

            nameTextView = (TextView) view.findViewById(R.id.tv_movie_name);
            releaseDateTextView = (TextView) view.findViewById(R.id.tv_release_year);
            genreTextView = (TextView) view.findViewById(R.id.tv_genre);
        }

    }



}
