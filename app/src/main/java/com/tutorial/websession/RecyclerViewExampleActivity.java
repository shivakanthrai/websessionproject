package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tutorial.websession.adapter.MovieAdapter;
import com.tutorial.websession.model.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shivakanthrai on 16/06/17.
 */

public class RecyclerViewExampleActivity extends AppCompatActivity {

    RecyclerView sampleRecyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview_exmple);
        sampleRecyclerView = (RecyclerView) findViewById(R.id.rv_sample);
        layoutManager =  new LinearLayoutManager(this);
        sampleRecyclerView.setLayoutManager(layoutManager);
        adapter = new MovieAdapter(this,generateData());
        sampleRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private List<Movie> generateData() {

        List<Movie> movies =  new ArrayList<>();

            Movie movie1 = new Movie("Gaurdians Of The Galaxy","2017","Comedy,Action");
            Movie movie2 = new Movie("Raabta","2017","Romance");
            Movie movie3 = new Movie("3 Idiots","2012","Comedy");
            Movie movie4 = new Movie("2 States","2016","Drama,Romance");
            Movie movie5 = new Movie("Wonder Woman","2017","Action");
            Movie movie6 = new Movie("Dangal","2016","Drama,Biopic");
            Movie movie7 = new Movie("Kirik Party","2017","Comedy,Drama");
            Movie movie8 = new Movie("Lord of the Rings","2014","Fantasy,Action");
            Movie movie9 = new Movie("Avangers","2014","Action");
            Movie movie10 = new Movie("Something","2017","Comedy,Action");

        movies.add(movie1);
        movies.add(movie2);
        movies.add(movie3);
        movies.add(movie4);
        movies.add(movie5);
        movies.add(movie6);
        movies.add(movie7);
        movies.add(movie8);
        movies.add(movie9);
        movies.add(movie10);

        return movies;
    }
}
