package com.tutorial.websession.network;

/**
 * Created by shivakanthrai on 27/06/17.
 */

public class NetworkConstants {

    public static final String BASE_URL = "https://www.yengram.com/";


    public static final String HELLO_SERVER_URL ="/example/sayhello" ;
    public static final String UPLOAD_PATIENT_URL = "/example/senddata";


    public static final int HELLO_SERVER_API_ID = 1;
    public static final int UPLOAD_PATIENT_API_ID = 2;

}
