package com.tutorial.websession;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.tutorial.websession.contentprovider.SampleContentProvider;

/**
 * Created by shivakanthrai on 10/06/17.
 */

public class SampleContentProviderActivity extends AppCompatActivity {

    private EditText wordEditText;
    private Button addWordButton;
    private ListView wordsListView;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_content_provider_tutorial);

        wordEditText = (EditText) findViewById(R.id.et_new_word);
        addWordButton = (Button) findViewById(R.id.bt_add_word);
        wordsListView = (ListView) findViewById(R.id.lv_words);

        adapter = new SimpleCursorAdapter(getBaseContext(),
                R.layout.layout_dictionary_row,
                null,
                new String[] { "name"},
                new int[] { R.id.tv_dictionary_word }, 0);

        wordsListView.setAdapter(adapter);
        refreshValuesFromContentProvider();

        addWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addWordInDictionary();
            }
        });
    }

    private void refreshValuesFromContentProvider() {
        Cursor c = getContentResolver().query(SampleContentProvider.CONTENT_URI,null,null,null,null);
        c.moveToFirst();
        while (c.moveToNext()){
            String value = c.getString(1);
            Log.i(getClass().getName(),"Custom Content -> "+ value);
        }
        //adapter.swapCursor(c);
    }


    private void addWordInDictionary() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",wordEditText.getText().toString());

        Uri uri = getContentResolver().insert(SampleContentProvider.CONTENT_URI, contentValues);
        refreshValuesFromContentProvider();
    }
}
