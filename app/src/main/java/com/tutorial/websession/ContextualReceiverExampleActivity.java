package com.tutorial.websession;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tutorial.websession.receivers.SampleBroadcastReceiver;

/**
 * Created by shivakanthrai on 06/06/17.
 */

public class ContextualReceiverExampleActivity extends AppCompatActivity {

    public static final String SAMPLE_ACTION = "com.tutorial.websession.TEST_ACTION";
    private SampleBroadcastReceiver sampleBroadcastReceiver;

    private EditText nameEditText;
    private Button sendButton;

    @Override
    protected void onStart() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Do something for lollipop and above versions
        } else{
            // do something for phones running an SDK before lollipop
        }


        sampleBroadcastReceiver = new SampleBroadcastReceiver();
        IntentFilter sampleBroadcastReceiverIntentFilter =  new IntentFilter();
        sampleBroadcastReceiverIntentFilter.addAction(SAMPLE_ACTION);
        //registerReceiver(sampleBroadcastReceiver,sampleBroadcastReceiverIntentFilter);
        //LocalBroadcastManager.getInstance(this).registerReceiver(sampleBroadcastReceiver,sampleBroadcastReceiverIntentFilter);
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contextual_receiver_tutorial);

        nameEditText = (EditText) findViewById(R.id.et_name);
        sendButton = (Button) findViewById(R.id.bt_send);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSampleBroadcast();
            }
        });
    }

    private void sendSampleBroadcast() {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SAMPLE_ACTION);
        broadcastIntent.putExtra("name",nameEditText.getText().toString());
        sendBroadcast(broadcastIntent, Manifest.permission.ACCESS_NETWORK_STATE);


       // LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }


    @Override
    protected void onStop() {
        unregisterReceiver(sampleBroadcastReceiver);
        super.onStop();
    }
}


