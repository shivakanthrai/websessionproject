package com.tutorial.websession.model;

/**
 * Created by shivakanthrai on 16/06/17.
 */

public class Movie {

    private String name;
    private String yearOfRelease;
    private String genre;


    public  Movie(String name,String yearOfRelease,String genre){
        this.name = name;
        this.yearOfRelease = yearOfRelease;
        this.genre = genre;
    }


    public String getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(String yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
