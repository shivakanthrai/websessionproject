package com.tutorial.websession;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tutorial.websession.second.SecondActivity;

public class MainActivity extends AppCompatActivity {

    public static final String MESSAGE_KEY = "MESSAGES";
    private static final String LOCATION_KEY = "LOCATION_KEY";
    private EditText nameTextView;
    private TextView locationTextView;
    private Button goButton;


    // Just Testing
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onCreate()");
        setContentView(R.layout.activity_main);

        intializeUIElements();
        addEventListeners();

        if(savedInstanceState != null){
            savedInstanceState.containsKey(LOCATION_KEY);
            locationTextView.setText(savedInstanceState.getString(LOCATION_KEY));
        }
    }



    private void intializeUIElements() {
        nameTextView = (EditText) findViewById(R.id.et_name);
        goButton = (Button) findViewById(R.id.bt_go_button);
        locationTextView = (TextView) findViewById(R.id.tv_display_location);


    }

    private void addEventListeners() {
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCustomHelloActivity();
            }
        });
    }

    private void openCustomHelloActivity() {
        String name = nameTextView.getText().toString();
        Log.i(getClass().getName(),name);

        Intent customMessageIntent = new Intent(this, SecondActivity.class);
        customMessageIntent.putExtra(MESSAGE_KEY,name);
        startActivityForResult(customMessageIntent,345);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            switch (requestCode){
                case 345:{
                    String location  = data.getStringExtra("location");
                    locationTextView.setText(location);
                }
            }
        }



    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onPause()");


       // Log.i
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onDestroy()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if(!locationTextView.getText().toString().trim().equals("")){
            outState.putString(LOCATION_KEY,locationTextView.getText().toString());
        }

    }
}
