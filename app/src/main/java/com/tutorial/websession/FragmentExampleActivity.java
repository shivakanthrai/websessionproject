package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.tutorial.websession.fragment.OtherSampleFragment;
import com.tutorial.websession.fragment.SampleFragment;

/**
 * Created by shivakanthrai on 14/06/17.
 */

public class FragmentExampleActivity extends AppCompatActivity implements SampleFragment.SampleFragmentListener {

    private SampleFragment sampleFragment;
    private OtherSampleFragment otherSampleFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_fragment_example);

        if(savedInstanceState == null){
            addSampleFragment();
        }else{
            sampleFragment = (SampleFragment) getSupportFragmentManager().findFragmentByTag(SampleFragment.class.getName());
            otherSampleFragment = (OtherSampleFragment) getSupportFragmentManager().findFragmentByTag(OtherSampleFragment.class.getName());
        }

    }

    void addSampleFragment(){

        sampleFragment = new SampleFragment();
        FragmentTransaction transaction =  getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.ll_fragment_container,sampleFragment,SampleFragment.class.getName());

        otherSampleFragment = new OtherSampleFragment();
        transaction.add(R.id.ll_other_fragment_container,otherSampleFragment,OtherSampleFragment.class.getName());
        transaction.commit();


    }


    @Override
    public void sayHello(String message) {
        otherSampleFragment.receiveMessage(message);
    }
}
