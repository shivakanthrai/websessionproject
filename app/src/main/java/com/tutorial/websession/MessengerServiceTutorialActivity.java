package com.tutorial.websession;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tutorial.websession.service.SampleBinderService;
import com.tutorial.websession.service.SampleMessengerService;

/**
 * Created by shivakanthrai on 03/06/17.
 */

public class MessengerServiceTutorialActivity extends AppCompatActivity{

    private TextView numberTextView;
    private Button executeButton;
    private boolean isBound;
    private Messenger messenger;

    private Messenger replyMessenger  = new Messenger(new MessengerServiceTutorialActivity.MessageHandler());



    class MessageHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what){
                case SampleMessengerService.MSG_TYPE_HELLO:{
                    Long randomNumber = msg.getData().getLong("RANDOM");
                    numberTextView.setText(""+randomNumber);
                }
                default:{
                    super.handleMessage(msg);
                }
            }
        }
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_bound_task_tutorial_activity);
        initalizeUIElements();
        addEventListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent bindingIntent = new Intent(this,SampleMessengerService.class);
        bindService(bindingIntent,serviceConnection,BIND_AUTO_CREATE);
    }

    private void initalizeUIElements() {

        numberTextView = (TextView) findViewById(R.id.tv_number);
        executeButton = (Button) findViewById(R.id.bt_execute);
    }

    private void addEventListeners() {
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateRandomNumber();
            }
        });
    }

    private void generateRandomNumber() {
        Message message = Message.obtain();
        message.what  = SampleMessengerService.MSG_TYPE_HELLO;
        message.replyTo = replyMessenger;

        try {
            messenger.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }



    @Override
    protected void onStop() {

        if(isBound){
            unbindService(serviceConnection);
            isBound =false;
        }
        super.onStop();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            messenger = new Messenger(iBinder);
            isBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBound = false;
        }
    };

}
