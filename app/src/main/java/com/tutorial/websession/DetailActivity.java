package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tutorial.websession.fragment.DetailFragment;

/**
 * Created by shivakanthrai on 15/06/17.
 */

public class DetailActivity extends AppCompatActivity{

    private DetailFragment detailFragment;
    private String selectedValue;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_detail_activity);

        selectedValue = getIntent().getStringExtra("selectedValue");
    }

    @Override
    protected void onResume() {
        super.onResume();
        detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.fr_detail);
        detailFragment.showSelectedValue(selectedValue);
    }
}
