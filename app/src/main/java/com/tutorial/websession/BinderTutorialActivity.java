package com.tutorial.websession;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tutorial.websession.service.SampleBinderService;

/**
 * Created by shivakanthrai on 03/06/17.
 */

public class BinderTutorialActivity extends AppCompatActivity {

    private TextView numberTextView;
    private Button executeButton;
    private SampleBinderService.SampleBinder sampleBinder;
    private boolean isBound;
    private SampleBinderService sampleBinderService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_bound_task_tutorial_activity);
        initalizeUIElements();
        addEventListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent bindingIntent = new Intent();
        bindingIntent.setAction(Intent.ACTION_SEND);
        bindingIntent.setType("text/plain");


        startActivity(bindingIntent);
    }

    private void initalizeUIElements() {

        numberTextView = (TextView) findViewById(R.id.tv_number);
        executeButton = (Button) findViewById(R.id.bt_execute);
    }

    private void addEventListeners() {
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateRandomNumber();
            }
        });
    }

    private void generateRandomNumber() {
        Long randomNumber = sampleBinderService.getRandomValue();
        numberTextView.setText(""+randomNumber);
    }


    @Override
    protected void onStop() {

        if(isBound){
            unbindService(serviceConnection);
            isBound =false;
        }
        super.onStop();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            sampleBinder = (SampleBinderService.SampleBinder) iBinder;
            isBound = true;
            sampleBinderService = sampleBinder.getService();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBound = false;
        }
    };

}
