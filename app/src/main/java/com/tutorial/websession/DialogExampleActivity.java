package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tutorial.websession.dialog.SampleDialogFragment;

/**
 * Created by shivakanthrai on 12/07/17.
 */

public class DialogExampleActivity extends AppCompatActivity implements SampleDialogFragment.SampleDialogFragmentListener {

    private Button showDialogButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_example);

        showDialogButton = (Button) findViewById(R.id.bt_show_dialog);

        showDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
    }

    private void showDialog() {

        SampleDialogFragment sampleDialogFragment = new SampleDialogFragment();
        sampleDialogFragment.setSampleDialogFragmentListener(this);
        sampleDialogFragment.show(getSupportFragmentManager(),"");



    }

    @Override
    public void onColorPick(String color) {
        ((TextView)findViewById(R.id.tv_selected_color)).setText(color);
    }
}
