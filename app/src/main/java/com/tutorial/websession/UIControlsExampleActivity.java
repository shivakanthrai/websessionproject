package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

/**
 * Created by shivakanthrai on 17/06/17.
 */

public class UIControlsExampleActivity extends AppCompatActivity {

    RadioGroup colorRadioGroup;
    TextView selectedColorTextView;
    CheckBox sampleCheckBox;
    TextView isItClearTextView;
    Button valuesButton;
    TextView finalValuesTextView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_ui_controls);

        colorRadioGroup = (RadioGroup) findViewById(R.id.rg_color);
        selectedColorTextView = (TextView) findViewById(R.id.tv_selected_color);
        sampleCheckBox = (CheckBox) findViewById(R.id.cb_sample);
        isItClearTextView = (TextView) findViewById(R.id.tv_is_it_clear);
        valuesButton = (Button) findViewById(R.id.bt_value);
        finalValuesTextView = (TextView) findViewById(R.id.tv_final_values);

        colorRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {


            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                RadioButton radioButton = (RadioButton) UIControlsExampleActivity.this.findViewById(i);
                selectedColorTextView.setText("Selected color is : "+radioButton.getText());
            }
        });


        sampleCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    isItClearTextView.setText("The students said 'Yes'");
                }else{
                    isItClearTextView.setText("The students said 'No'");
                }
            }
        });


        valuesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String finalValues = "";

                if(sampleCheckBox.isChecked()){
                    finalValues+=  "The students said 'Yes' \n";
                }else{
                    finalValues+=  "The students said 'No' \n";
                }

                int id = colorRadioGroup.getCheckedRadioButtonId();

                if(id == -1){
                    //No value selected so do what you need to in that scenario
                }else{
                    RadioButton radioButton = (RadioButton) UIControlsExampleActivity.this.findViewById(id);
                    finalValues+=  "Selected color is : "+radioButton.getText();
                }


                finalValuesTextView.setText(finalValues)    ;

            }
        });

    }


}
