package com.tutorial.websession.asynctask;

import android.os.AsyncTask;

/**
 * Created by shivakanthrai on 31/05/17.
 */

public class SampleAsyncTask extends AsyncTask<String,String,String> {

    private SampleAsynTaskListener taskListener;

    public SampleAsyncTask(SampleAsynTaskListener taskListener){
        this.taskListener = taskListener;
    }

    @Override
    protected void onPreExecute() {
        taskListener.startingTaskExecution();

    }

    @Override
    protected String doInBackground(String... params) {
        Integer timeInSecs = Integer.parseInt(params[0]);
        long count = (timeInSecs*10000000L);

        for(long i = 0; i < count;i++){

        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        taskListener.taskCompleted(result);
    }
}
