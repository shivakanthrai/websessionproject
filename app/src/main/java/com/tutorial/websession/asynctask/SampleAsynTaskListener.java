package com.tutorial.websession.asynctask;

/**
 * Created by shivakanthrai on 31/05/17.
 */

public interface SampleAsynTaskListener {
    void startingTaskExecution();
    void showProgress(String progressMessage);
    void taskCompleted(String result);
}
