package com.tutorial.websession.asynctask;

/**
 * Created by shivakanthrai on 03/07/17.
 */

public interface GeocoderListener {

    void displayAddress(String address);
}
