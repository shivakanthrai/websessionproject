package com.tutorial.websession.asynctask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by shivakanthrai on 03/07/17.
 */

public class GeocoderAsyncTask extends AsyncTask<Location,String,String>  {

    GeocoderListener geocoderListener;
    Context context;


    public  GeocoderAsyncTask(Context context,GeocoderListener geocoderListener){
        this.geocoderListener = geocoderListener;
        this.context = context;
    }

    @Override
    protected String doInBackground(Location[] locations) {
        String errorMessage = "";
        Geocoder geocoder = new Geocoder(this.context, Locale.getDefault());

        Location location = locations[0];

        // ...

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = "Service Not Available";
        } catch (IllegalArgumentException illegalArgumentException) {
            errorMessage = "Invalid lat,long";
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = "no address found";
            }
            return errorMessage;
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            return  TextUtils.join(System.getProperty("line.separator"),
                            addressFragments);
        }
    }

    @Override
    protected void onPostExecute(String address) {
        geocoderListener.displayAddress(address);
    }
}
