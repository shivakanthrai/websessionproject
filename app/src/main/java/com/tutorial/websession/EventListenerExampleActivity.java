package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by shivakanthrai on 17/06/17.
 */

public class EventListenerExampleActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button1;
    private Button button2;
    private Button button3;
    private TextView messageTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_lister_example_activity);

        button1 = (Button) findViewById(R.id.bt_button1);
        button2 = (Button) findViewById(R.id.bt_button2);
        button3 = (Button) findViewById(R.id.bt_button3);
        messageTextView = (TextView) findViewById(R.id.tv_message);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        messageTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.bt_button1:{
                messageTextView.setText("Button 1 was clicked!");
                break;
            }

            case R.id.bt_button2:{
                messageTextView.setText("Button 2 was clicked!");
                break;
            }

            case R.id.bt_button3:{
                messageTextView.setText("Button 3 was clicked!");
                break;
            }

            case R.id.tv_message:{
                messageTextView.setText("Why would you tap on a text view when there are three buttons?!!!");
                break;
            }
        }
    }
}
