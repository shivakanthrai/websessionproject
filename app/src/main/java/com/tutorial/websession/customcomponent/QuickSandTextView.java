package com.tutorial.websession.customcomponent;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.tutorial.websession.R;

/**
 * Created by shivakanthrai on 21/06/17.
 */

public class QuickSandTextView extends AppCompatTextView {

    private Typeface bold;
    private Typeface light;
    private Typeface medium;
    private Typeface regular;


    public QuickSandTextView(Context context) {
        super(context);
        initialiseFontTypeFace();
    }

    public QuickSandTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialiseFontTypeFace();

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.QuickSandTextView);
        int fontType = typedArray.getInt(R.styleable.QuickSandTextView_custom_font_type,1);
        setFontTypeFace(fontType);


    }

    public QuickSandTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialiseFontTypeFace();
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.QuickSandTextView);
        int fontType = typedArray.getInt(R.styleable.QuickSandTextView_custom_font_type,1);
        setFontTypeFace(fontType);
    }

    private void initialiseFontTypeFace(){
        bold = Typeface.createFromAsset(getContext().getAssets(),"font/Quicksand-Bold.ttf");
        light = Typeface.createFromAsset(getContext().getAssets(),"font/Quicksand-Light.ttf");
        medium = Typeface.createFromAsset(getContext().getAssets(),"font/Quicksand-Medium.ttf");
        regular = Typeface.createFromAsset(getContext().getAssets(),"font/Quicksand-Regular.ttf");
    }

    private void setFontTypeFace(int fontType){
        switch (fontType){
            case 1:{
                setTypeface(bold);
                break;
            }

            case 2:{
                setTypeface(medium);
                break;
            }

            case 3:{
                setTypeface(light);
                break;
            }

            case 4:{
                setTypeface(regular);
                break;
            }

        }
    }

}
