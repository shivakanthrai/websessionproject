package com.tutorial.websession.customcomponent;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tutorial.websession.R;

import java.util.ArrayList;

/**
 * Created by shivakanthrai on 21/06/17.
 */

public class Rater extends LinearLayout implements View.OnClickListener {

    private static final int DEFAULT_MAX_RATING = 5;
    int numberOfStars = DEFAULT_MAX_RATING;
    ArrayList<ImageView> stars = new ArrayList<>();

    public Rater(Context context) {
        super(context);
        layoutUI();

    }

    public Rater(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setValuesFromUI(attrs);
        layoutUI();
    }

    public Rater(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setValuesFromUI(attrs);
        layoutUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Rater(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        setValuesFromUI(attrs);
        layoutUI();
    }

    private void setValuesFromUI(AttributeSet attrs) {

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs,R.styleable.Rater);
        numberOfStars = typedArray.getInt(R.styleable.Rater_maxRating,DEFAULT_MAX_RATING);
    }

    private void layoutUI() {
        setOrientation(HORIZONTAL);

        for (int i= 0; i < numberOfStars;i++){
            ImageView imageView = new ImageView(getContext());
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(layoutParams);
            imageView.setImageResource(R.drawable.star);
            imageView.setOnClickListener(this);
            imageView.setTag(i);
            stars.add(imageView);
            addView(imageView);

        }
    }


    @Override
    public void onClick(View view) {

        try{
            int index = (Integer) view.getTag();

            int i = 0;
            for(; i <= index; i++){
                ImageView star = stars.get(i);
                star.setImageResource(R.drawable.star_selected);
            }


           for (;i<stars.size();i++){
                ImageView star = stars.get(i);
                star.setImageResource(R.drawable.star);
            }

        }catch (Exception e){

        }




    }
}
