package com.tutorial.websession;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.tutorial.websession.fragment.DetailFragment;
import com.tutorial.websession.fragment.MasterFragment;

/**
 * Created by shivakanthrai on 15/06/17.
 */

public class MasterDetailExampleActivity extends AppCompatActivity implements MasterFragment.MasterFragmentListener{

    FrameLayout frameLayout;
    boolean isLandscapeMode = false;
    private DetailFragment detailFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_master_detail_activity);

        frameLayout = (FrameLayout) findViewById(R.id.fl_detail_fragment_container);

        if(frameLayout == null){
            isLandscapeMode = false;
        }else{
            isLandscapeMode = true;
        }


        if(savedInstanceState == null){
            displayFragments();
        }else{
            detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentByTag(DetailFragment.class.getName());

            if(detailFragment == null){
                displayFragments();
            }
        }
    }

    private void displayFragments() {
        if(isLandscapeMode){
            detailFragment = new DetailFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fl_detail_fragment_container,detailFragment,DetailFragment.class.getName());
            transaction.commit();
        }
    }

    @Override
    public void selectedValue(String value) {

        if(isLandscapeMode){
            detailFragment.showSelectedValue(value);
        }else{
            Intent detailActivityIntent = new Intent(this,DetailActivity.class);
            detailActivityIntent.putExtra("selectedValue",value);
            startActivity(detailActivityIntent);
        }

    }
}
