package com.tutorial.websession;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


/**
 * Created by shivakanthrai on 29/05/17.
 */

public class LifeCycleDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_lifecycle_demo);
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onCreate()");

    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onStop()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(getClass().getSimpleName(),"LifeCycle Logger -> onDestroy()");
    }
}
