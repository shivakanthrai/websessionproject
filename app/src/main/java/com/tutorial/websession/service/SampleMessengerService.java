package com.tutorial.websession.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import java.util.Random;

/**
 * Created by shivakanthrai on 03/06/17.
 */

public class SampleMessengerService extends Service{

    public static final int MSG_TYPE_HELLO = 1;
    private Random randomGenertor = new Random();
    private Messenger messenger  = new Messenger(new MessageHandler());



    class MessageHandler extends Handler{

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what){
                case MSG_TYPE_HELLO:{
                    Message message = Message.obtain();
                    message.what = MSG_TYPE_HELLO;
                    message.getData().putLong("RANDOM",randomGenertor.nextLong());

                    try {
                        msg.replyTo.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                default:{
                    super.handleMessage(msg);
                }
            }
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }
}
