package com.tutorial.websession.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import java.util.Random;

/**
 * Created by shivakanthrai on 03/06/17.
 */

public class SampleBinderService extends Service{

    private Random randomGenerator = new Random();

    public class SampleBinder extends Binder{

        public SampleBinderService getService(){
            return SampleBinderService.this;
        }
    }


    private SampleBinder sampleBinder = new SampleBinder();

    @Override
    public void onCreate() {
        //logger
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent,int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return sampleBinder;
    }


    public long getRandomValue(){
       return randomGenerator.nextLong();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
