package com.tutorial.websession.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by shivakanthrai on 02/06/17.
 */

public class SampleIntentService extends IntentService {

    public SampleIntentService(){
        super("SampleIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Integer timeInSecs = Integer.parseInt("5");
        long count = (timeInSecs*10000000L);

        for(long i = 0; i < count;i++){

        }

    }

    //1. Creates a separate worker thread
    //2. Creates a queue from all the intents coming in
    //3.Automatically stops the service when there are no intents to process
    //4.Default onBind implementation which returns a null value.
}
