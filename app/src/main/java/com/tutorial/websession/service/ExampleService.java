package com.tutorial.websession.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

/**
 * Created by shivakanthrai on 02/06/17.
 */

public class ExampleService extends Service {

    private Handler sampleHandler;

    class SampleHandler extends Handler{

        public SampleHandler(Looper looper){
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            int startId = msg.arg1;
            Integer timeInSecs = Integer.parseInt("5");
            long count = (timeInSecs*10000000L);

            for(long i = 0; i < count;i++){

            }

            stopSelf(startId);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {

        HandlerThread sampleThread = new HandlerThread("SampleHandlerThread"){
            @Override
            public void run() {
                Message message = sampleHandler.obtainMessage();
                message.arg1 = startId;
                sampleHandler.sendMessage(message);
            }
        };

        sampleHandler = new SampleHandler(sampleThread.getLooper());
        sampleThread.start();

        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void runDummyTimeConsumingTask(int startId) {

    }
}
