package com.tutorial.websession.second;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tutorial.websession.MainActivity;
import com.tutorial.websession.R;


/**
 * Created by shivakanthrai on 27/05/17.
 */

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    TextView messageTextView;
    private EditText locationTextView;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_second_activity);
        intializeUIElements();
        addEventListeners();


        //Display Message
        String  name = getIntent().getStringExtra(MainActivity.MESSAGE_KEY);

        if(name != null){
            messageTextView.setText(getString(R.string.hello) + " "+ name+"!");
        }

    }

    private void intializeUIElements() {
        messageTextView = (TextView) findViewById(R.id.tv_message);
        locationTextView = (EditText) findViewById(R.id.et_location);
        sendButton = (Button) findViewById(R.id.bt_go_button);

    }

    private void addEventListeners() {
        sendButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.bt_go_button:{
                sendDataBack();
            }
        }

    }

    private void sendDataBack() {

        Intent sendingIntent = new Intent();
        sendingIntent.putExtra("location",locationTextView.getText().toString());
        setResult(RESULT_OK,sendingIntent);
        finish();
    }
}
