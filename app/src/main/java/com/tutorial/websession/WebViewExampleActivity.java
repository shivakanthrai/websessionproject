package com.tutorial.websession;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

/**
 * Created by shivakanthrai on 07/07/17.
 */

public class WebViewExampleActivity extends AppCompatActivity {

    private WebView webView;
    private Button backButton;
    private Button forwardButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_example);

        webView = (WebView) findViewById(R.id.wv_webview);
        backButton = (Button) findViewById(R.id.bt_back);
        forwardButton = (Button) findViewById(R.id.bt_forward);


        webView.setWebViewClient(new CustomWebViewClient());
        webView.loadUrl("http://www.google.in");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.goBack();
            }
        });


        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.goForward();
            }
        });

    }

    private void setNavigationButtonStates() {
        if(!webView.canGoBack()){
            backButton.setEnabled(false);
        }else{
            backButton.setEnabled(true);
        }

        if(!webView.canGoForward()){
            forwardButton.setEnabled(false);
        }else {
            forwardButton.setEnabled(true);
        }
    }



    class CustomWebViewClient extends WebViewClient{

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            setNavigationButtonStates();
        }

    }
}
