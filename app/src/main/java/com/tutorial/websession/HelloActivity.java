package com.tutorial.websession;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by shivakanthrai on 23/06/17.
 */

public class HelloActivity extends AppCompatActivity{

    private TextView helloTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_hello_activity);

        helloTextView = (TextView) findViewById(R.id.tv_hello);

        Intent intent = getIntent();

        if(intent.hasExtra("name")) {
            helloTextView.setText(intent.getStringExtra("name") + " says hello");

        }
    }
}
